const http = require('http');
const httpProxy = require('http-proxy');
const jwt = require('jsonwebtoken');
const proxy = httpProxy.createProxyServer({});

let EventHost = 'http://localhost:8000';
let AccountsHost = 'http://localhost:5000';
let SecretKey = 'Super Secret Key';
let EventsKey = 'Events Super Secret Key';

if (process.env.ENV === 'Production') {
  EventHost = process.env.EventHost;
  AccountsHost = process.env.AccountsHost;
  SecretKey = process.env.SecretKey;
  EventsKey = process.env.EventsKey;
}

proxy.on('proxyReq', async function (proxyReq, req, res, options) {
  if(req.url.startsWith("/events")) {
    proxyReq.setHeader('SecretKey', EventsKey);
    if (req.headers.authorization) {
      let token = req.headers.authorization.substr(7);
      await jwt.verify(token, SecretKey, function (err, decoded) {
        if (err) {
          res.setHeader("Access-Control-Allow-Origin", "*");
          if (err.name === "TokenExpiredError")
            res.statusCode = 455;
          else
            res.statusCode = 401;
          res.write(err.name);
          res.connection.end();
          return res.end();
        } else {
          if (!(typeof decoded.role === 'string'))
            decoded.role = decoded.role.join(",");
          proxyReq.setHeader('authorization', JSON.stringify(decoded));
        }
      });
    }
  }
});

function getTarget(url) {
  if (url.startsWith('/events')) {
    return EventHost;
  }
  if (url.startsWith('/accounts')) {
    return AccountsHost;
  }
  return 'http://localhost:1000';
}

var server = http.createServer(async function (req, res) {
  proxy.web(req, res, { target: getTarget(req.url) }, function (err) {
    res.statusCode = 500;
    res.write(err.message);
    res.end();
  });
});

console.log('Listening on 80');
server.listen(80);
